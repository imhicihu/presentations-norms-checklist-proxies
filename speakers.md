# Speakers


* Pointer
![pointer](https://bitbucket.org/repo/z88jp6x/images/1082631380-pointer_english.png)
----------------------


* Point the pointer at the receiver next to the notebook to advance or rewind the slides
![Point the pointer at the receiver](https://bitbucket.org/repo/z88jp6x/images/2439232385-puntero-laser-usb-inalambrico-para-presentaciones-d_nq_np_659883-mec25923434575_082017-f.png)
![receiver](https://bitbucket.org/repo/z88jp6x/images/4240775973-IMG_0114.JPG)