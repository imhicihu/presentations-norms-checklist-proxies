# Ponentes


* Puntero
![puntero](https://bitbucket.org/repo/z88jp6x/images/3494526752-puntero_spanish.png)
----------------------


* Apunte el puntero al receptor situado junto a la computadora para avanzar o retroceder las filminas
![apunte puntero al receptor](https://bitbucket.org/repo/z88jp6x/images/2439232385-puntero-laser-usb-inalambrico-para-presentaciones-d_nq_np_659883-mec25923434575_082017-f.png)
![receptor](https://bitbucket.org/repo/z88jp6x/images/4240775973-IMG_0114.JPG)
