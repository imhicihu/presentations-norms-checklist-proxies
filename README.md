![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# Rationale #

### What is this repository for? ###

* Quick summary
	- A kind of _checklist_ when we create a webinar, meeting or colloquium. This is a common effort between parties: different languages, operating systems, countries or time zones, _et alia_
 ![presentación.jpeg](https://bitbucket.org/repo/z88jp6x/images/1535152636-4240775973-IMG_0114.jpg)

### How do I get set up? ###

* Summary of set up & configuration
	- Verify our:
		* [Speakers](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/speakers.md)
		* [Ponentes](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/ponentes.md)
		* [Colophon](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/Colophon.md)
		* [Procedures](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/Procedures.md)
		* [Presentaciones](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/Presentaciones.md)

* Dependencies
	- Check our [colophon](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/Colophon.md)
* Database configuration
	- There is no database, instead, this is a _massive_ checklist that encompass our needs
* Deployment instructions
	- Verify and test our [streaming guidelines](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/streaming_guidelines.md)
	- Check our [file transfers](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/file_transfers.md)
	- Check our [colophon](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/Colophon.md)
	- Check our [Ipad](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/Ipad_stuff.md) section

### Related repositories ###

* Some repositories linked with this project:
     - [Conferences](https://bitbucket.org/imhicihu/conferences/src/)
	 - [Proxy settings](https://bitbucket.org/imhicihu/proxy-settings-tutorials/src/master/)
	 - [Proxy access](https://bitbucket.org/imhicihu/proxy-access/src/master/)
	 - [QR Code](https://bitbucket.org/imhicihu/qr-code/src/)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/issues)

### Who do I talk to? ###

* Repo owner or admin
	 - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/addon/trello/trello-board) of this repo.
(You need a [Trello](https://trello.com/) account)

### Legal ###

* All trademarks are the property of their respective owners. 